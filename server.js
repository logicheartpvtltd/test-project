// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

//Disable the CORF
app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080; // set our port

var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost:27017/contacts'); // connect to our database
var Bear     = require('./app/models/users'); 
var Industry     = require('./app/models/industry');
var Clientaccounts     = require('./app/models/clientaccount');

// ROUTES FOR OUR API
// =============================================================================

// create our router
// checking
// checking twice
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'Welcome to the node' });	
});

// on routes that starts in /users
// ----------------------------------------------------
router.route('/users')

	// create a users (accessed at POST http://localhost:8080/users)
	.post(function(req, res) {
		
		var bear = new Bear();		// create a new instance of the Users model
		bear.name = req.body.name;  // set the Users name (comes from the request)
		bear.age = req.body.age;	// set the Users age (comes from the request)
		bear.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'User Created!' });
		});

		
	})

	// get all the users (accessed at GET http://localhost:8080/api/users)
	.get(function(req, res) {
		Bear.find(function(err, bears) {
			if (err)
				res.send(err);

			res.json(bears);
		});
	});
// on routes that end in /users

// on routes that starts in /accounts
// ----------------------------------------------------
router.route('/accounts')

	// create a users (accessed at POST http://localhost:8080/accounts)
	.post(function(req, res) {
		
		var accounts = new Clientaccounts();		// create a new instance of the Client accounts model

		//Clients account details starts here  (comes from the request)
		accounts.company_name = req.body.company_name; 
		accounts.industry_id = req.body.industry_id;
		accounts.email = req.body.email;
		accounts.phone = req.body.phone;
		accounts.country = req.body.country;
		accounts.state = req.body.state;
		accounts.city = req.body.city;
		accounts.zip_code = req.body.zip_code;
		accounts.address = req.body.address;

		//Clients account details ends here  (comes from the request)

		accounts.save(function(err) {
			if (err)
				res.send(err);

			res.json({ staus: true, result: 'Account Created!' });
		});

		
	})

	// get all the users (accessed at GET http://localhost:8080/api/accounts)
	.get(function(req, res) {
		Clientaccounts.find(function(err, clientaccounts) {
			if (err)
				res.send(err);

			res.json({status: true, result: clientaccounts});
		});
	});

// on routes that end in /users/:bear_id
// ----------------------------------------------------
router.route('/accounts/:_id')
	
	// get the Client Account with that id
	.get(function(req, res) {
		// the user was found and is available in req.user
    	//res.send('What is up ' + req.params.account_id + '!');
		//var account = new Clientaccounts();
		Clientaccounts.find({_id: req.params._id}, function(err, clientaccount) {
			if (err)
				res.send(err);
			res.json(clientaccount);
		});
	})

	// update the User with this id
	.put(function(req, res) {
		Bear.findById(req.params.bear_id, function(err, bear) {

			if (err)
				res.send(err);

			bear.name = req.body.name;
			bear.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Account updated!' });
			});

		});
	})

	// delete the User with this id
	.delete(function(req, res) {
		Bear.remove({
			_id: req.params.bear_id
		}, function(err, bear) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
	});


// on routes that end in /accounts

// ----------------------------------------------------
router.route('/industries')

	
	// create a users (accessed at POST http://localhost:8080/industries)
	.post(function(req, res) {
		
		var industry = new Industry();		// create a new instance of the Users model
		industry.name = req.body.name;  // set the Users name (comes from the request)
		industry.save(function(err) {
			if (err)
				res.send(err);

			res.json({ status: true, result: 'Industry Created!' });
		});

		
	})
	// get all the users (accessed at GET http://localhost:8080/api/users)
	.get(function(req, res) {
		Industry.find(function(err, industries) {
			if (err)
				res.send(err);
			var obj = { status: true, result : industries };
			res.json(obj);
		});
	});

// on routes that end in /users/:bear_id
// ----------------------------------------------------
router.route('/users/:user_id')

	// get the User with that id
	.get(function(req, res) {
		var industry = new Industry();
		Users.findById(req.params.user_id, function(err, bear) {
			if (err)
				res.send(err);
			res.json(Users);
		});
	})

	// update the User with this id
	.put(function(req, res) {
		Bear.findById(req.params.bear_id, function(err, bear) {

			if (err)
				res.send(err);

			bear.name = req.body.name;
			bear.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Bear updated!' });
			});

		});
	})

	// delete the User with this id
	.delete(function(req, res) {
		Bear.remove({
			_id: req.params.bear_id
		}, function(err, bear) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
	});


// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
