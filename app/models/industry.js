var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var IndustrySchema   = new Schema({
	name: String
});

module.exports = mongoose.model('Industry', IndustrySchema);