var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

//Custome email validating function
var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

//Mongoose Schema for Client account
var clientaccountSchema   = new Schema({
	company_name: {
		type: String,
		required: true,
		trim: true,
		maxlength: 2
	},
	industry_id: {
		type: String,
		requird: true
	},
	email: {
		type: String,
		lowercase: true,
		trim: true,
		validate: [validateEmail, 'Please fill a valid email address'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
		required: true
	},
	phone: {
		type: Number,
		min: 3,
		required: true
	},
	country: {
		type: String,
		trim: true,
		required: true
	},
	state: {
		type: String,
		trim: true,
		required: true
	},
	city: {
		type: String,
		trim: true,
		required: true
	},
	zip_code: {
		type: String,
		trim: true,
		required: true
	},
	address: {
		type: String,
		trim: true,
		required: true
	},
	created_date: {
		type: Date,
		default: Date.now
	} 
});

//Custome error message for comapny name
clientaccountSchema.path('company_name').validate(function(company_name) {
  return company_name && company_name.length > 2;
}, 'Company Name should be more than two charectors.');

module.exports = mongoose.model('clientaccount', clientaccountSchema);